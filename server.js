var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  const config = {
	   llave : "Practitioner&JWT&&*"
  };
  app.set('llave', config.llave);

var jwt=require('jsonwebtoken');

var path = require('path');

var requestjson = require('request-json');

var urlMlabRaiz="https://api.mlab.com/api/1/databases/oordonez/collections/";
var apiKey="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz
var clienteMlabMov

var bodyparser=require("body-parser");
app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json({limit:'10mb'}))
//app.use(bodyparser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token");
  next();
})

app.listen(port);


app.post('/v2/Login',function(req, res){

  var email=req.body.email;
  var password=req.body.password;
  let query='q={"email":"'+ email +'","password":"'+ password +'"}';
  //console.log(query);

  clienteMlabRaiz=requestjson.createClient(urlMlabRaiz + "Usuarios?"+ apiKey + "&" + query);
  //console.log(clienteMlabRaiz);
  clienteMlabRaiz.get('',function(err, resM, body){
    if (!err) {
      //console.log(" body "+JSON.stringify(body));
      if (body.length==1) {//login ok
        var tokenData = {
          username: req.body.email
        }
        var token=jwt.sign(tokenData,app.get('llave'));// expires in 24 hours
        res.jsonp({succes: true, message:'Authentication successful', token: token});
      }else {
        res.status(404).jsonp({ error: 'Los datos ingresados son incorrectos/El usuario no existe, registrese'});
      }
    }
  });
});

app.post('/v2/SignUp',function(req, res){
  var email=req.body.email;
  var password=req.body.password;
  let query='q={"email":"'+ email +'","password":"'+ password +'"}';
  data={
            "email" : email,
            "password" : password,
    }
  clienteMlabRaiz=requestjson.createClient(urlMlabRaiz + "Usuarios?"+ apiKey + "&" + query);
  clienteMlabRaiz.get('',function(err, resM, body){
    if (!err) {
      if (body.length==1) {
        res.jsonp({ "error": 'El Usuario ya se encuentra registrado'});
      }else {

        clienteMlabRaiz=requestjson.createClient(urlMlabRaiz + "Usuarios?"+ apiKey );
        //console.log(clienteMlabRaiz)
        clienteMlabRaiz.post('',data,function(err, resM, body){
               res.send({"msg" : "Usuario registrado satisfactoriamente"});
             });
      }
    }
  });


    });

const rutasProtegidas = express.Router();
rutasProtegidas.use((req, res, next) => {
    const token = req.headers['access-token'];

    if (token) {
      jwt.verify(token, app.get('llave'), (err, decoded) => {
        if (err) {
          return res.json({ mensaje: 'Token inválida' });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.send({
          mensaje: 'Token no proveída.'
      });
    }
 });

 app.get('/v2/Movimientos/:user', rutasProtegidas,function(req,res){
   let query2='q={"email":"'+ req.params.user +'"}';
    //console.log(query2);
   clienteMlabMov=requestjson.createClient(urlMlabRaiz + "Usuarios?"+ apiKey + "&" + query2);
   //console.log(clienteMlabMov);
   clienteMlabMov.get('',function(err, resM, body){
     if (!err) {
       if (body.length==1) {//datos del cliente
         res.jsonp(body[0].listado);
         //console.log("respuesta"+JSON.stringify(body[0].email));
       }else {
         res.status(404).jsonp({ error: 'No hay movimientos registrados'});
       }
     }
   });
   //res.json(movimientosJSON);
 });


app.post('/v3/Movimientos/:email',  rutasProtegidas, function(req,res) {
  var destino=req.body.destino;
  var imagen=req.body.imagen;
  var costo=req.body.costo;
  var fecha= new Date();
  var email=req.params.email;
  let query='q={"email":"'+ email+'"}';
  //console.log(email);
  data={ $push: {"listado":
        	{
            "country" : destino,
            "cost" : costo,
            "image": imagen,
            "date": fecha.toDateString()+" "+fecha.getHours()+":"+fecha.getMinutes()+''
        	}
    	}
    }
  clienteMlabRaiz=requestjson.createClient(urlMlabRaiz + "Usuarios?"+ apiKey + "&" + query);
  clienteMlabRaiz.put('',data,function(err, resM, body){
    //console.log(body.n)
    if (!err) {
      if (body.n==1) {
        res.status(200).jsonp({succes: true, message:'Movimiento agregado satisfactoriamente'});
      }else {
        res.status(404).jsonp({ error: 'No se agrego el movimiento'});
      }
    }
  });
});
